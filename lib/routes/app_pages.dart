import 'package:firebase/dashboard/dashboard_binding.dart';
import 'package:firebase/dashboard/dashboard_page.dart';
import 'package:get/get.dart';

import 'app_routes.dart';

class AppPages {
  static final list = [
    GetPage(
      name: AppRoutes.DASHBOARD,
      page: () => DashboardPage(),
      binding: DashboardBinding(),
    ),
  ];
}
