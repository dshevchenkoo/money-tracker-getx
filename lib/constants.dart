import 'package:flutter/material.dart';

const defaultPadding = 16.0;
const primaryColor = Color.fromRGBO(144, 83, 235, 1);
const lightGrey = Color.fromRGBO(208, 208, 208, 1);
const unactiveColor = Color.fromRGBO(97, 97, 97, 1);
const greyColor = Colors.grey;
