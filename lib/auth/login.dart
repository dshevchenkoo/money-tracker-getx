import 'package:email_validator/email_validator.dart';
import 'package:firebase/auth/service/auth_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:firebase/auth/login_controller.dart';
import 'package:firebase/constants.dart';

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final LoginController loginController = Get.put(LoginController());
    return Scaffold(
        body: Center(
            child: Column(
      children: [
        Expanded(
            child: Padding(
          padding: const EdgeInsets.only(top: defaultPadding * 5),
          child: getHeader(),
        )),
        Expanded(
            child: Obx(() => loginController.isSignUpView.value
                ? getAuthForm('Регистрация')
                : getAuthForm('Войти'))),
        Obx(() => loginController.isSignUpView.value
            ? getQuestToggleButton('Уже есть аккаунт?', 'Войти')
            : getQuestToggleButton('Ещё нет аккаунта?', 'Регистрация'))
      ],
    )));
  }

  Row getQuestToggleButton(quest, buttonName) {
    final LoginController loginController = Get.put(LoginController());

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(quest),
        TextButton(
            onPressed: () {
              loginController.toggleAuthPage();
            },
            child: Text(buttonName))
      ],
    );
  }

  Widget getAuthForm(buttonName) {
    final LoginController loginController = Get.find();
    final AuthController authController = Get.put(AuthController());

    GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    TextEditingController _fieldEmailController = TextEditingController();
    TextEditingController _fieldPasswordController = TextEditingController();

    void _submitForm(buttonName) {
      if (_formKey.currentState!.validate()) {
        authController.submitForm(buttonName);
      }
    }

    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(defaultPadding * 2),
            child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    Obx(() => Focus(
                          onFocusChange: (hasFocus) {
                            loginController.changeEmailColor(hasFocus
                                ? Rx<Color>(primaryColor)
                                : Rx<Color>(unactiveColor));
                          },
                          child: TextFormField(
                            validator: (value) {
                              if (value == '') {
                                return 'Заполните поле E-mail';
                              }
                              if (!EmailValidator.validate(value!)) {
                                return 'Проверьте формат ввода поля E-mail';
                              }
                            },
                            controller: authController.emailController,
                            decoration: InputDecoration(
                                labelText: 'E-mail',
                                labelStyle: TextStyle(
                                  color: loginController.emailLabelColor.value,
                                )),
                          ),
                        )),
                    Obx(() => Focus(
                          onFocusChange: (hasFocus) {
                            loginController.changePasswordColor(hasFocus
                                ? Rx<Color>(primaryColor)
                                : Rx<Color>(unactiveColor));
                          },
                          child: TextFormField(
                            validator: (value) {
                              if (value == '') {
                                return 'Заполните поле Пароль';
                              }
                              if (value!.length < 6) {
                                return 'Пароль должен быть длиннее 6 символов';
                              }
                            },
                            controller: authController.passwordController,
                            obscureText: !loginController.showPassword.value,
                            decoration: InputDecoration(
                                labelText: 'Пароль',
                                labelStyle: TextStyle(
                                  color:
                                      loginController.passwordLabelColor.value,
                                )),
                          ),
                        )),
                    Row(
                      children: [
                        Obx(() => Checkbox(
                              activeColor: primaryColor,
                              value: loginController.showPassword.value,
                              onChanged: (_) {
                                loginController.toggleShowPassword();
                              },
                            )),
                        Text('Показать пароль')
                      ],
                    ),
                  ],
                )),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: defaultPadding * 2),
            child: SizedBox(
                width: double.infinity,
                child: Obx(() => ElevatedButton(
                    onPressed: authController.isLoading.isFalse
                        ? () {
                            _submitForm(buttonName);
                          }
                        : null,
                    child: authController.isLoading.isFalse
                        ? Text(buttonName)
                        : SizedBox(
                            width: 20,
                            height: 20,
                            child: CircularProgressIndicator(
                              color: primaryColor,
                              strokeWidth: 3,
                            ))))),
          )
        ],
      ),
    );
  }

  Widget getHeader() {
    return Column(
      children: [
        Container(child: Image.asset('assets/icons/logo.png')),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: defaultPadding),
          child: Text(
            'Учёт расходов',
            style: TextStyle(
                fontWeight: FontWeight.bold, letterSpacing: -1, fontSize: 25),
          ),
        ),
        Container(
          width: 225,
          child: Text(
            'Ваша история расходов всегда под рукой',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 15),
          ),
        )
      ],
    );
  }
}
