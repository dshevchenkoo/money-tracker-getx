// import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:firebase/constants.dart';

class LoginController extends GetxController {
  Rx<bool> isSignUpView = true.obs;

  var passwordLabelColor = unactiveColor.obs;
  var emailLabelColor = unactiveColor.obs;

  Rx<bool> showPassword = false.obs;

  changePasswordColor(color) {
    passwordLabelColor.value = color.value;
  }

  changeEmailColor(color) {
    emailLabelColor.value = color.value;
  }

  toggleShowPassword() {
    showPassword.value = !showPassword.value;
  }

  toggleAuthPage() {
    isSignUpView.value = !isSignUpView.value;
  }
}
