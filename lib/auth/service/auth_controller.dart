import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class AuthController extends GetxController {
  final FirebaseAuth auth = FirebaseAuth.instance;

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  RxBool isLoading = false.obs;

  submitForm(String buttonName) {
    final action = {
      'Регистрация': signUp,
      'Войти': signIn,
    };
    isLoading.toggle();
    action[buttonName]!();
  }

  Future<bool> signUp() async {
    try {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: emailController.text, password: passwordController.text);
      isLoading.toggle();
      return true;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        snackAction('Такой пароль слишком слабый!');
        isLoading.toggle();
      } else if (e.code == 'email-already-in-use') {
        snackAction('Такой E-mail уже зарегистрирован в системе');
        isLoading.toggle();
      }
      return false;
    } catch (e) {
      print(e);
      isLoading.toggle();
      return false;
    }
  }

  Future<bool> signIn() async {
    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: emailController.text, password: passwordController.text);
      isLoading.toggle();
      return true;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        snackAction('Пользователь с таким E-mail не найден');
        isLoading.toggle();
      } else if (e.code == 'wrong-password') {
        snackAction('Кажется допущена ошибка в Пароле');
        isLoading.toggle();
      }
      return false;
    }
  }

  Future<void> signOut() async => await FirebaseAuth.instance.signOut();

  // FirebaseAuth.instance.authStateChanges().listen((User? user) {
  //     if (user == null) {
  //       print('User is currently signed out!');
  //     } else {
  //       print('User is signed in!');
  //     }
  //   });
  Stream<User?> authStateChanges() {
    return FirebaseAuth.instance.authStateChanges();
  }

  snackAction(String message) {
    Get.snackbar(
      'Ой...',
      message,
      duration: Duration(seconds: 4),
      animationDuration: Duration(milliseconds: 800),
      snackPosition: SnackPosition.TOP,
    );
  }

  isAuth() {
    final currentUser = FirebaseAuth.instance.currentUser;
    return currentUser != null ? true : false;
  }
}
