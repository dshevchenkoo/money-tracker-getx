import 'package:firebase/auth/service/auth_controller.dart';
import 'package:firebase/constants.dart';
import 'package:firebase/home/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomePage extends GetView<HomeController> {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final AuthController authContoller = Get.find();

    return Container(
      child: Scaffold(
        appBar: AppBar(
            centerTitle: true,
            toolbarHeight: 120,
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    top: defaultPadding * 3, right: defaultPadding * 1.5),
                child: IconButton(
                  splashRadius: 25,
                  padding: EdgeInsets.zero,
                  icon: Icon(Icons.add),
                  color: Colors.white,
                  onPressed: () {
                    controller.addCategory(context);
                  },
                ),
              )
            ],
            title: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: defaultPadding * 3),
                  child: InkWell(
                    onTap: () {
                      controller.selectDate(context);
                    },
                    child: Obx(() => Text(
                          controller.getCurrentDate(),
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 25),
                        )),
                  ),
                ),
              ],
            )),
        body: Column(
          children: [
            Expanded(
                flex: 2,
                child: Container(
                  color: lightGrey,
                  child: Center(
                    child: Text('За Сентябрь нет расходов'),
                  ),
                )),
            Expanded(
                flex: 3,
                child: Obx(() => Text(controller.selectedDate.toString()))),
          ],
        ),
      ),
    );
  }
}
