import 'package:firebase/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

import 'dart:math' as math;

class CategoryController extends GetxController {
  void addCategory(context) {
    TextEditingController _nameController = TextEditingController();
    TextEditingController _colorController = TextEditingController();

    GlobalKey<FormState> _formKey = GlobalKey<FormState>();

    showDialog(
        context: context,
        builder: (_) => AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              title: Center(
                  child: Text(
                'Добавить категорию',
                style: TextStyle(fontWeight: FontWeight.bold),
              )),
              content: Container(
                height: 255,
                child: SingleChildScrollView(
                  child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          TextFormField(
                            controller: _nameController,
                            decoration: InputDecoration(labelText: 'Название'),
                            validator: (value) {
                              if (value == '') {
                                return 'Введите Название';
                              }
                            },
                          ),
                          Stack(
                            alignment: Alignment.centerRight,
                            children: <Widget>[
                              TextFormField(
                                maxLength: 6,
                                controller: _colorController,
                                decoration: InputDecoration(
                                  labelText: 'Цвет',
                                ),
                                validator: (value) {
                                  if (value == '') {
                                    return 'Укажите Цвет';
                                  }
                                },
                              ),
                              IconButton(
                                splashRadius: 25,
                                color: unactiveColor,
                                icon: Icon(Icons.palette),
                                onPressed: () {
                                  _showColor(context, _colorController);
                                },
                              ),
                            ],
                          ),
                          SizedBox(
                            height: defaultPadding,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: defaultPadding),
                            child: SizedBox(
                                width: double.infinity,
                                child: ElevatedButton(
                                    onPressed: () {
                                      addCategorySubmit(_formKey);
                                    },
                                    child: Text('Добавить'))),
                          ),
                          Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: defaultPadding),
                              child: SizedBox(
                                  width: double.infinity,
                                  child: TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Text('Отмена'))))
                        ],
                      )),
                ),
              ),
            ));
  }

  void _showColor(context, TextEditingController colorController) {
    bool isColorChanged = false;

    Color currentColor =
        Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0);

    var getStringColor = (Color color) {
      String colorName = color.toString(), startColorName = 'Color(0xff';
      return colorName.substring(startColorName.length).replaceAll(')', '');
    };

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            titlePadding: const EdgeInsets.all(0.0),
            contentPadding: const EdgeInsets.all(0.0),
            content: SingleChildScrollView(
              child: ColorPicker(
                pickerColor: currentColor,
                onColorChanged: (Color color) {
                  isColorChanged = true;
                  colorController.text = getStringColor(color);
                },
                colorPickerWidth: 300.0,
                pickerAreaHeightPercent: 0.7,
                enableAlpha: true,
                displayThumbColor: true,
                showLabel: true,
                paletteType: PaletteType.hsv,
                pickerAreaBorderRadius: const BorderRadius.only(
                  topLeft: const Radius.circular(2.0),
                  topRight: const Radius.circular(2.0),
                ),
              ),
            ),
            actions: [
              Center(
                child: TextButton(
                    onPressed: () {
                      if (!isColorChanged) {
                        colorController.text = getStringColor(currentColor);
                      }
                      Navigator.of(context).pop();
                    },
                    child: Text('Выбрать')),
              )
            ],
          );
        });
  }

  addCategorySubmit(GlobalKey<FormState> formKey) {
    if (formKey.currentState!.validate()) {
      print('Успех');
    }
  }
}
