import 'package:firebase/home/controllers/category_controller.dart';
import 'package:firebase/home/home_binding.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class HomeController extends GetxController {
  @override
  void onInit() {
    HomeBinding().dependencies();
    super.onInit();
  }

  final Rx<DateTime> selectedDate = DateTime.now().obs;

  String getCurrentDate() {
    final String month = monthes[selectedDate.value.month]!,
        year = selectedDate.value.year.toString();

    return "$month $year";
  }

  static const Map<int, String> monthes = {
    1: 'Январь',
    2: 'Февраль',
    3: 'Март',
    4: 'Апрель',
    5: 'Май',
    6: 'Июнь',
    7: 'Июль',
    8: 'Август',
    9: 'Сентябрь',
    10: 'Октябрь',
    11: 'Ноябрь',
    12: 'Декабрь',
  };

  Future<void> selectDate(BuildContext context) async {
    final DateTimeRange? pickedDate = await showDateRangePicker(
        context: context,
        locale: const Locale('ru', 'RU'),
        currentDate: DateTime.now(),
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));
    // final DateTime? pickedDate = await showDatePicker(
    //     locale: const Locale('ru', 'RU'),
    //     context: context,
    //     initialDate: DateTime.now(),
    //     firstDate: DateTime(2015),
    //     lastDate: DateTime(2050));
    // if (pickedDate != null && pickedDate != DateTime.now()) {
    //   selectedDate.value = pickedDate;
    //   print(pickedDate.month);
    // }
  }

  addCategory(context) {
    CategoryController categoryController = Get.find();
    categoryController.addCategory(context);
  }
}
