import 'package:flutter/material.dart';
import 'package:firebase/constants.dart';

int _fullAlpha = 255;
Color blueDark = new Color.fromARGB(_fullAlpha, 51, 92, 129);
Color blueLight = new Color.fromARGB(_fullAlpha, 116, 179, 206);
Color yellow = new Color.fromARGB(_fullAlpha, 252, 163, 17);
Color red = new Color.fromARGB(_fullAlpha, 255, 85, 84);
Color green = new Color.fromARGB(_fullAlpha, 59, 178, 115);

Color activeIconColor = yellow;

ThemeData themeData = ThemeData.light().copyWith(
    colorScheme: ColorScheme.light().copyWith(
      primary: primaryColor,
    ),
    primaryColor: primaryColor,
    accentColor: primaryColor,
    textSelectionTheme: TextSelectionThemeData(cursorColor: primaryColor),
    inputDecorationTheme: InputDecorationTheme(
      focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: primaryColor, width: 1.0)),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.grey, width: 0.0),
      ),
    ),
    textButtonTheme:
        TextButtonThemeData(style: TextButton.styleFrom(primary: primaryColor)),
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
      padding: EdgeInsets.symmetric(vertical: 14),
      primary: primaryColor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    )));
