import 'package:firebase/auth/service/auth_controller.dart';
import 'package:firebase/constants.dart';
import 'package:firebase/dashboard/dashboard_controller.dart';
import 'package:firebase/home/home_page.dart';
import 'package:firebase/profile/profile_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DashboardPage extends GetView<DashboardController> {
  @override
  Widget build(BuildContext context) {
    final AuthController authContoller = Get.find();
    return Scaffold(
      body: Obx(() => IndexedStack(
            index: controller.tabIndex.value,
            children: [HomePage(), ProfilePage()],
          )),
      bottomNavigationBar: Obx(() => BottomNavigationBar(
              selectedItemColor: primaryColor,
              onTap: controller.changeTabIndex,
              currentIndex: controller.tabIndex.value,
              items: [
                _bottomNavigationBarItem(
                  icon: CupertinoIcons.creditcard,
                  label: 'Расходы',
                ),
                _bottomNavigationBarItem(
                  icon: CupertinoIcons.person_fill,
                  label: 'Профиль',
                ),
              ])),
    );
  }

  _bottomNavigationBarItem({IconData? icon, String? label}) {
    return BottomNavigationBarItem(
      icon: Icon(icon),
      label: label,
    );
  }
}
