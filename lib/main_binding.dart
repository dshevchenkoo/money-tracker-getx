import 'package:firebase/auth/service/auth_controller.dart';
import 'package:firebase/dashboard/dashboard_controller.dart';
import 'package:firebase/home/home_controller.dart';
import 'package:get/get.dart';

class MainBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DashboardController());
    Get.lazyPut(() => HomeController());
    Get.lazyPut(() => AuthController());
  }
}
