import 'package:firebase/auth/service/auth_controller.dart';
import 'package:firebase/profile/profile_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProfilePage extends GetView<ProfileController> {
  final AuthController authContoller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: TextButton(
              onPressed: authContoller.signOut, child: Text('Выйти'))),
    );
  }
}
