import 'package:firebase/auth/service/auth_controller.dart';
import 'package:firebase/dashboard/dashboard_page.dart';
import 'package:firebase/main_binding.dart';
import 'package:firebase/routes/app_pages.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:firebase/auth/login.dart';
import 'package:firebase/theme.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  MainBinding().dependencies();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final AuthController authController = Get.find();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.white, // Color for Android
        statusBarBrightness:
            Brightness.dark // Dark == white status bar -- for IOS.
        ));
    return Listener(
      onPointerDown: (_) {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.focusedChild?.unfocus();
        }
      },
      child: GetMaterialApp(
          title: 'Учёт расходов',
          initialBinding: MainBinding(),
          theme: themeData,
          getPages: AppPages.list,
          localizationsDelegates: [GlobalMaterialLocalizations.delegate],
          supportedLocales: [const Locale('en'), const Locale('ru', 'RU')],
          home: SafeArea(
              child: StreamBuilder(
            stream: Get.find<AuthController>().authStateChanges(),
            builder: (BuildContext context, AsyncSnapshot<User?> snapshot) {
              if (snapshot.data == null) {
                return Login();
              } else {
                return DashboardPage();
              }
            },
          ))),
    );
  }
}
